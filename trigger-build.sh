#!/bin/sh

cd ~/beastix-build
export BEASTIX_LOCAL"=`pwd`/../beastix"
export BEASTIX_GITLAB="git@gitlab.com:beastix/beastix.git"

git clone --bare $BEASTIX_LOCAL
cd beastix.git
git push --mirror $BEASTIX_GITLAB
cd ..
rm -rf beastix.git
