This project is used only for the Beastix build infrastructure.

How it works:
  Clone Beastix repo
  Do stuff
  Configure variables in trigger-build.sh
  Run trigger-build.sh
  Beastix repo is pushed to gitlab
  Gitlab CI notices the commit and tries a build
